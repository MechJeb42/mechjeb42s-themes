#!/bin/sh
sed -i \
         -e 's/#251c30/rgb(0%,0%,0%)/g' \
         -e 's/#6BA0D4/rgb(100%,100%,100%)/g' \
    -e 's/#632f73/rgb(50%,0%,0%)/g' \
     -e 's/#5677e4/rgb(0%,50%,0%)/g' \
     -e 's/#3d434f/rgb(50%,0%,50%)/g' \
     -e 's/#d3dae3/rgb(0%,0%,50%)/g' \
	"$@"
